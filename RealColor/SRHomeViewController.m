//
//  SRHomeViewController.m
//  RealColor
//
//  Created by sunhong on 2017/3/28.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRHomeViewController.h"
#import "SRSettingViewController.h"
#import "SRFavoriteViewController.h"
#import "SRSwitchViewController.h"
#import "SRRunModePickView.h"
#import "SRDiskSelectView.h"
#import "SRDataBaseManger.h"
#import "LeadLampManager.h"
#import "SRVerSliderView.h"
#import "SRRGBChangeView.h"
#import "WFDiskView.h"

#define  TopNavHeight   ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 88 : 64)
#define  BottomHeight   ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 83 : 49)
#define  CURRENTFRAME  CGRectMake(0, TopNavHeight, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - TopNavHeight - BottomHeight)
#define RGBSENDVALUE  @"rgbSendValue"

@interface SRHomeViewController ()<WFDiskViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *toolBarButtons;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIView *diskBgView;
@property (weak, nonatomic) IBOutlet UIButton *runButton;
@property (weak, nonatomic) IBOutlet UIButton *brightnessBtn;
@property (weak, nonatomic) IBOutlet UIButton *rgbButton;
@property (weak, nonatomic) IBOutlet UIButton *whiteButton;

@property (nonatomic, strong) SRSettingViewController *settingVC;
@property (nonatomic, strong) SRFavoriteViewController *favoriteVC;
@property (nonatomic, strong) SRSwitchViewController *switchVC;

@property (nonatomic, assign) SRLEDColorType type;
@property (nonatomic, strong) UIView *containView;
@property (nonatomic, strong) UIButton *selectedBtn;
@property (nonatomic, assign) BOOL isDiskSelectionViewShow;
@property (nonatomic, strong) SRRunModePickView *modePickView;
@property (nonatomic, strong) SRVerSliderView *rgbBbrightnessView;
@property (nonatomic, strong) SRVerSliderView *cctBbrightnessView;
@property (nonatomic, strong) SRVerSliderView *whiteView;
@property (nonatomic, strong) SRRGBChangeView *rgbView;
@property (nonatomic, strong) SRDiskSelectView *diskSelectView;
@property (nonatomic, strong) WFDiskView *rgbDiskView;
@property (nonatomic, strong) WFDiskView *cctDiskView;
@property (nonatomic, strong) WFDiskView *dimDiskView;

@end

@implementation SRHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _homeButton.selected = YES;
    _selectedBtn = _homeButton;
    [self addChildViewControllers];
    
    self.title = NSLocalizedString(@"RealColor", nil);
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"disk_selection"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarClicked)];
    
    self.type = SRLEDColorTypeRGB;
    [self loadDiskView];
}

- (void)addChildViewControllers{
    _settingVC = [[SRSettingViewController alloc] init];
    _favoriteVC = [[SRFavoriteViewController alloc] init];
    _switchVC = [[SRSwitchViewController alloc] init];
    _settingVC.view.frame = CURRENTFRAME;
    _favoriteVC.view.frame = CURRENTFRAME;
    _switchVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - BottomHeight);
    [self addChildViewController:_settingVC];
    [self addChildViewController:_favoriteVC];
    [self addChildViewController:_switchVC];
}

#pragma mark -- lazy load view

- (UIView *)containView{
    if(!_containView){
        _containView = [[UIView alloc]initWithFrame:CURRENTFRAME];
        UIImageView *bgImageView = [[UIImageView alloc]initWithFrame:_containView.bounds];
        bgImageView.image = [UIImage imageNamed:@"close_background"];
        [_containView addSubview:bgImageView];
    }
    return _containView;
}

- (SRRGBChangeView *)rgbView{
    if(!_rgbView){
        _rgbView = [SRRGBChangeView loadSRRGBChangeView];
    }
    return _rgbView;
}

- (SRVerSliderView *)rgbBbrightnessView{
    if(!_rgbBbrightnessView){
        _rgbBbrightnessView = [self createBrightnessView];
    }
    return _rgbBbrightnessView;
}

- (SRVerSliderView *)cctBbrightnessView{
    if(!_cctBbrightnessView){
        _cctBbrightnessView = [self createBrightnessView];
    }
    return _cctBbrightnessView;
}

- (SRVerSliderView *)createBrightnessView{
    SRVerSliderView *brightnessView = [[SRVerSliderView alloc]initWithFrame:CGRectMake(self.brightnessBtn.frame.origin.x, self.runButton.frame.origin.y + 44 + 16, _runButton.frame.size.width, CGRectGetMaxY(_whiteButton.frame) - CGRectGetMaxY(_runButton.frame) - 44 - 32) andTransValue:0.5];

    return brightnessView;
}

- (SRVerSliderView *)whiteView{
    if(!_whiteView){
        _whiteView = [[SRVerSliderView alloc]initWithFrame:CGRectMake(self.brightnessBtn.frame.origin.x, self.runButton.frame.origin.y + 44 + 16, _runButton.frame.size.width, CGRectGetMaxY(_whiteButton.frame) - CGRectGetMaxY(_runButton.frame) - 44 - 32) andTransValue:1.5];
    }
    return _whiteView;
}

- (SRRunModePickView *)modePickView{
    if(!_modePickView){
        _modePickView = [[SRRunModePickView alloc]initWithFrame:CGRectMake(0, self.runButton.frame.origin.y + 44 + 16, self.view.frame.size.width, self.rgbButton.frame.origin.y - CGRectGetMaxY(self.runButton.frame) - 32)];
    }
    return _modePickView;
}

- (SRDiskSelectView *)diskSelectView{
    if(!_diskSelectView){
        _diskSelectView = [SRDiskSelectView loadDiskSelectView];
        _diskSelectView.frame = CURRENTFRAME;
        __weak typeof(self) weakSelf = self;
        [_diskSelectView setSelectTypeBlock:^(SRLEDColorType type) {
            weakSelf.type = type;
            [weakSelf loadDiskView];
            weakSelf.isDiskSelectionViewShow = NO;
        }];
        [_diskSelectView setViewDidRemoveBlock:^{
            weakSelf.isDiskSelectionViewShow = NO;
        }];

    }
    return _diskSelectView;
}

- (WFDiskView *)rgbDiskView{
    if(!_rgbDiskView){
        _rgbDiskView = [self creatDiskView:@"disk_rgb" limiteValue:96];
        _rgbDiskView.delegate = self;
    }
    return _rgbDiskView;
}

- (WFDiskView *)cctDiskView{
    if(!_cctDiskView){
        _cctDiskView = [self creatDiskView:@"disk_cct" limiteValue:33];
        _cctDiskView.delegate = self;
    }
    return _cctDiskView;
}

- (WFDiskView *)dimDiskView{
    if(!_dimDiskView){
        _dimDiskView = [self creatDiskView:@"disk_dim" limiteValue:255];
        _dimDiskView.delegate = self;
    }
    return _dimDiskView;
}

- (WFDiskView *)creatDiskView:(NSString *)imageName limiteValue:(NSInteger)limiteValue{
    CGFloat bigWidth = 250 * self.view.frame.size.width / 375.0;
    CGFloat smallWidth = 220 * bigWidth / 250.0;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, bigWidth, bigWidth)];
    imageView.image = [UIImage imageNamed:imageName];
    UIImageView *activedView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, smallWidth, smallWidth)];
    activedView.image = [UIImage imageNamed:@"disk_front"];
    WFDiskView *diskView= [[WFDiskView alloc] initWithFrame:CGRectMake(0, 0, bigWidth, bigWidth) activedView:activedView backgroundView:imageView limitedValue:limiteValue];
    diskView.circleAllowed = YES;
    return diskView;
}

#pragma mark -- right button click

- (void)rightBarClicked{
    [self resetAllView];
    _isDiskSelectionViewShow = !_isDiskSelectionViewShow;
    if(_isDiskSelectionViewShow){
        [self.view addSubview:self.diskSelectView];
    }else{
        [_diskSelectView removeFromSuperview];
        _diskSelectView = nil;
    }
}

#pragma mark -- toolBar button click

- (IBAction)barButtonClicked:(UIButton *)sender {
    
    if(sender.tag == 4){
        sender.selected = !sender.selected;
        _selectedBtn.selected = NO;
    }else{
        if(sender.selected){
            return;
        }else{
            sender.selected = !sender.selected;
            _selectedBtn.selected = NO;
            _selectedBtn = sender;
        }
    }
    switch (sender.tag) {
        case 1:{
            self.title = NSLocalizedString(@"RealColor", nil);
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"disk_selection"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarClicked)];
            [_favoriteVC.view removeFromSuperview];
            [_settingVC.view removeFromSuperview];

            break;
        }
            
        case 2:{
            self.title = NSLocalizedString(@"Settings", nil);
            [_favoriteVC.view removeFromSuperview];
            
            [self.view addSubview:_settingVC.view];
            self.navigationItem.rightBarButtonItem = nil;
            break;
        }
            
        case 3:{
            
            self.title = NSLocalizedString(@"Favorite", nil);
            [_settingVC.view removeFromSuperview];
            
            [self.view addSubview:_favoriteVC.view];
            self.navigationItem.rightBarButtonItem = nil;
            break;

        }
        case 4:{
            if(sender.selected){
                self.navigationController.navigationBarHidden = YES;
                [self.view addSubview:_switchVC.view];
                LeadLampCommand *comd = [LeadLampCommand powerOff];
                [self sendCommd:comd];
                // off
                for (UIButton *btn in self.toolBarButtons) {
                    if(btn != sender){
                        btn.enabled = NO;
                    }
                }
            }else{
                self.navigationController.navigationBarHidden = NO;
                [_switchVC.view removeFromSuperview];
                // on
                for (UIButton *btn in self.toolBarButtons) {
                    btn.enabled = YES;
                }
                LeadLampCommand *comd = [LeadLampCommand powerOn];
                [self sendCommd:comd];
                _selectedBtn.selected = YES;
            }
            break;
        }
            
        default:
            break;
    }
}

- (void)loadDiskView{
    
    for (UIView *view in self.diskBgView.subviews) {
        [view removeFromSuperview];
    }
    switch (self.type) {
        case SRLEDColorTypeRGB:{
            [self.diskBgView addSubview:self.rgbDiskView];
            self.runButton.hidden = NO;
            self.rgbButton.hidden = NO;
            self.whiteButton.hidden = NO;
            self.brightnessBtn.hidden = NO;
            break;
        }
        case SRLEDColorTypeCCT:{
            [self.diskBgView addSubview:self.cctDiskView];
            self.runButton.hidden = YES;
            self.rgbButton.hidden = YES;
            self.whiteButton.hidden = YES;
            self.brightnessBtn.hidden = NO;
            break;
        }
        case SRLEDColorTypeDIM:{
            [self.diskBgView addSubview:self.dimDiskView];
            self.runButton.hidden = YES;
            self.rgbButton.hidden = YES;
            self.whiteButton.hidden = YES;
            self.brightnessBtn.hidden = YES;
            break;
        }
    }
}


#pragma MARK -- WFDiskViewDelegate

- (void)diskView:(WFDiskView *)diskView didUpdatedValue:(NSInteger)value{
    LeadLampCommand *cmd = nil;
    switch (self.type) {
        case SRLEDColorTypeRGB:{
            value = value - 48;
            if(value < 0){
                value = value + 96;
            }
            cmd = [LeadLampCommand rgbHue:value/95.0];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@(value) forKey:RGBSENDVALUE];
            [defaults synchronize];
            break;
       }
        case SRLEDColorTypeCCT:{
            cmd = [LeadLampCommand cctHue:value/32.0];
            break;
        }
            
        case SRLEDColorTypeDIM:{
            value = value + 1;
            cmd = [LeadLampCommand dimHue:value/254.0];
            break;
        }
    }
    NSLog(@"value---%d",value);
    [self sendCommd:cmd];
}

- (void)stopRunCommnd{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *value = [defaults objectForKey:RGBSENDVALUE];
    LeadLampCommand *cmd = [LeadLampCommand rgbHue:[value floatValue]/95.0];
    [self sendCommd:cmd];
}


- (IBAction)runClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        if(_rgbButton.selected){
            [self removeRGBView];
        }
        
        [self.view addSubview:self.containView];
        [self.view addSubview:self.modePickView];
        if(_brightnessBtn.selected){
            [self.view bringSubviewToFront:self.rgbBbrightnessView];
        }
        if(_whiteButton.selected){
            [self.view bringSubviewToFront:self.whiteView];
        }
        [self bringButtonsToFrontView];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *selectedMode = [defaults objectForKey:@"SelectedRunModel"];
        NSInteger mode;
        if(selectedMode){
            mode = [selectedMode integerValue];
        }else{
            mode = 1;
        }
        LeadLampCommand *comd = [LeadLampCommand rgbStartRun:mode];
        [self sendCommd:comd];
        
    }else{
        [self.containView removeFromSuperview];
        [_modePickView removeFromSuperview];
        [self stopRunCommnd];
    }
    
}

- (IBAction)brightness:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        if(_whiteButton.selected){
            _whiteButton.selected = NO;
            [_whiteView removeFromSuperview];
        }
        if(_rgbButton.selected){
            [self removeRGBView];
        }
        __block LeadLampCommand *comd;
        __weak typeof(self) weakSelf = self;
        SRVerSliderView *brightnessView;
        if(self.type == SRLEDColorTypeRGB){
            brightnessView = self.rgbBbrightnessView;
            [self.rgbBbrightnessView setSliderValueChange:^(CGFloat value) {
                comd = [LeadLampCommand rgbBrightness:value];
                [weakSelf sendCommd:comd];
            }];
        }else if (self.type == SRLEDColorTypeCCT){
            brightnessView = self.cctBbrightnessView;
            [self.cctBbrightnessView setSliderValueChange:^(CGFloat value) {
                comd = [LeadLampCommand cctBrightness:value];
                [weakSelf sendCommd:comd];
            }];
        }
        
        [self.view addSubview:brightnessView];
        
    }else{
        [self.rgbBbrightnessView removeFromSuperview];
        [self.cctBbrightnessView removeFromSuperview];
    }
}

- (IBAction)rgbChange:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        if(_runButton.selected){
            [_modePickView removeFromSuperview];
            [self.containView removeFromSuperview];
            _runButton.selected = NO;
            [self stopRunCommnd];
        }
        if(_whiteButton.selected){
            [_whiteView removeFromSuperview];
            _whiteButton.selected = NO;
        }
        if(_brightnessBtn.selected){
            [self.rgbBbrightnessView removeFromSuperview];
            _brightnessBtn.selected = NO;
        }
        
        self.rgbView.frame = CGRectMake(16, CGRectGetMaxY(self.runButton.frame) + 16, self.view.frame.size.width - 32, self.rgbButton.frame.origin.y - CGRectGetMaxY(self.runButton.frame) - 32);
        [self.view addSubview:self.containView];
        [self.view addSubview:_rgbView];
 
        [self bringButtonsToFrontView];
        
    }else{
        [self.containView removeFromSuperview];
        [_rgbView removeFromSuperview];
    }
}

- (IBAction)whiteChange:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        if(_brightnessBtn.selected){
            _brightnessBtn.selected = NO;
            [self.rgbBbrightnessView removeFromSuperview];
        }
        if(_rgbButton.selected){
            [self removeRGBView];
        }
        
        
        __weak typeof(self) weakSelf = self;
        [self.whiteView setSliderValueChange:^(CGFloat value) {
            LeadLampCommand *comd = [LeadLampCommand rgbWhite:value];
            [weakSelf sendCommd:comd];
        }];
        [self.view addSubview:_whiteView];
        
    }else{
        [_whiteView removeFromSuperview];
    }
}

- (void)bringButtonsToFrontView{
    [self.view bringSubviewToFront:self.runButton];
    [self.view bringSubviewToFront:self.rgbButton];
    [self.view bringSubviewToFront:self.brightnessBtn];
    [self.view bringSubviewToFront:self.whiteButton];
}

- (void)resetAllView{
    self.runButton.selected = NO;
    self.brightnessBtn.selected = NO;
    self.rgbButton.selected = NO;
    self.whiteButton.selected = NO;
    [_whiteView removeFromSuperview];
    [_modePickView removeFromSuperview];
    [self.containView removeFromSuperview];
    [_rgbView removeFromSuperview];
    [self.rgbBbrightnessView removeFromSuperview];
    [self.cctBbrightnessView removeFromSuperview];
}

- (void)removeRGBView{
    [_rgbView removeFromSuperview];
    _rgbButton.selected = NO;
    [self.containView removeFromSuperview];
}

- (void)sendCommd:(LeadLampCommand *)comd{
    [[LeadLampManager sharedInstance] sendCommand:comd toLamps:[[SRDataBaseManger sharedInstance] selectedLampsArray] completion:nil];
}

@end
