//
//  SRConnectController.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRConnectController.h"
#import "LeadLampManager.h"
#import "MBProgressHUD.h"

@interface SRConnectController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *wifiNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;

@end

@implementation SRConnectController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.desLabel.text = NSLocalizedString(@"connect_description", nil);
    self.wifiNameLabel.text = self.wifiNetwork.ssid;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
    _passwordTF.delegate = self;
}

- (void)done{
    [self.view endEditing:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LeadLampManager sharedInstance] connectToTheWifiNetwork:_wifiNetwork passwork:_passwordTF.text completed:^(NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:true];
            
            MBProgressHUD *hue = [MBProgressHUD showHUDAddedTo:self.view animated:true];
            hue.mode = MBProgressHUDModeText;
            hue.label.numberOfLines = 0;
            if (err) {
                NSLog(@"err %@", err.description);
                hue.label.text = NSLocalizedString(@"connect_fail", nil);
            }else {
                
                hue.label.text = [NSString stringWithFormat:NSLocalizedString(@"connect_success", nil), _wifiNetwork.ssid];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            hue.removeFromSuperViewOnHide = true;
            [hue hideAnimated:true afterDelay:2];

        });
        
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
