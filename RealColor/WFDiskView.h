//
//  WFDiskView.h
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-10-5.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WFDiskView;

@protocol WFDiskViewDelegate <NSObject>

- (void)diskView:(WFDiskView *)diskView didUpdatedValue:(NSInteger)value;

@end

@interface WFDiskView : UIControl

@property (nonatomic, weak) id<WFDiskViewDelegate> delegate;
@property (nonatomic, assign) BOOL circleAllowed;
@property (nonatomic, assign) BOOL actived;

- (id)initWithFrame:(CGRect)frame
        activedView:(UIView *)activedView
     backgroundView:(UIView *)backgroundView
       limitedValue:(NSInteger)limitedValue;

@end
