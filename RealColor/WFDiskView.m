//
//  WFDiskView.m
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-10-5.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import "WFDiskView.h"

@interface WFDiskView () {
    NSInteger _curValue;
    CGFloat _deltaAngle;
    CGFloat _angleOfDisk;
    CGAffineTransform _startTransform;
}

@property (nonatomic, strong) UIView *activedView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIImageView *offDiskView;
@property (nonatomic, assign) NSInteger limitedValue;
@property (nonatomic,assign) CGPoint origin;

@end

@implementation WFDiskView

- (id)initWithFrame:(CGRect)frame activedView:(UIView *)activedView backgroundView:(UIView *)backgroundView limitedValue:(NSInteger)limitedValue
{
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        
        self.activedView = activedView;
        self.backgroundView = backgroundView;
        self.limitedValue = limitedValue;
        
        backgroundView.frame = CGRectMake((frame.size.width - backgroundView.frame.size.width) / 2., (frame.size.height - backgroundView.frame.size.height) / 2., backgroundView.frame.size.width, backgroundView.frame.size.height);
        
        activedView.frame = CGRectMake((frame.size.width - activedView.frame.size.width) / 2., (frame.size.height - activedView.frame.size.height) / 2., activedView.frame.size.width, activedView.frame.size.height);
        
        [self addSubview:backgroundView];
        [self addSubview:activedView];
        
        self.origin = CGPointMake(frame.size.width / 2., frame.size.height / 2.);
        
        _actived = YES;
        self.offDiskView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, activedView.bounds.size.width * 3, activedView.bounds.size.height * 3)];
        _offDiskView.center = activedView.center;
        _offDiskView.userInteractionEnabled = YES;
     //   [_offDiskView setImage:[UIImage imageNamed:[@"off-beijing"] stretchableImageWithLeftCapWidth:0 topCapHeight:0]];
    }
    
    return self;
}

#pragma mark - Private


#pragma mark - touch methods

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_actived) {
        return;
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint previousTouchPoint = [touch previousLocationInView:self];
    CGPoint currentTouchPoint = [touch locationInView:self];
    
    CGFloat angleInRadians = atan2f(currentTouchPoint.y - _origin.y, currentTouchPoint.x - _origin.x) - atan2f(previousTouchPoint.y - _origin.y, previousTouchPoint.x - _origin.x);
    
    if (fabsf(angleInRadians) < 1.) {
        CGFloat tempAngleOfDisk = _angleOfDisk + angleInRadians;
        if (!_circleAllowed) {
            if (tempAngleOfDisk < 0 || tempAngleOfDisk > 2 * M_PI) {
                return;
            }
        }
        
        if (fabsf(tempAngleOfDisk) > 2 * M_PI) {
            if (tempAngleOfDisk < 0) {
                tempAngleOfDisk = -(fabsf(tempAngleOfDisk) - (int)(fabsf(tempAngleOfDisk) / (2 * M_PI)) * 2 * M_PI);
            } else {
                tempAngleOfDisk = fabsf(tempAngleOfDisk) - (int)(fabsf(tempAngleOfDisk) / (2 * M_PI)) * 2 * M_PI;
            }
        }
        if (tempAngleOfDisk < 0) {
            tempAngleOfDisk = 2 * M_PI - 2 * fabsf(tempAngleOfDisk) + fabsf(tempAngleOfDisk);
        }
        
        NSInteger currentCount = (int)(tempAngleOfDisk / ((2 * M_PI) / _limitedValue));
        
        _angleOfDisk += angleInRadians;
        
        
        [_activedView setTransform:CGAffineTransformRotate([_activedView transform], angleInRadians)];
        
        if (currentCount != _curValue && _delegate && [_delegate respondsToSelector:@selector(diskView:didUpdatedValue:)]) {
            [_delegate diskView:self didUpdatedValue:currentCount];
            _curValue = currentCount;
            //NSLog(@"%f %ld", atan2f(_activedView.transform.b, _activedView.transform.a), (long)currentCount);
        }
    }
}

- (void)setActived:(BOOL)actived
{
    _actived = actived;
    if (actived) {
        [_offDiskView removeFromSuperview];
    } else {
        [self insertSubview:_offDiskView atIndex:1];
    }
}

@end
