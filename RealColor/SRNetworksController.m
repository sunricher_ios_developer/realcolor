//
//  SRNetworksController.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRNetworksController.h"
#import "SRConnectController.h"
#import "SRNetworkCell.h"
#import "LeadLampManager.h"
#import "MBProgressHUD.h"

@interface SRNetworksController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *networks;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) UIButton *connectButton;
@property (nonatomic, assign) BOOL isProgressViewShow;
@end

@implementation SRNetworksController

- (UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 49) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
        imageView.image = [UIImage imageNamed:@"background"];
        _tableView.backgroundView = imageView;
        [_tableView registerNib:[UINib nibWithNibName:@"SRNetworkCell" bundle:nil] forCellReuseIdentifier:@"netCell"];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self reGetNetworks];
}

- (void)initUI{
    
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.tableView];
    self.title = NSLocalizedString(@"networks", nil);
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshNetWorks)];
    
    UIButton *connectBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 49, self.view.frame.size.width, 49)];
    
    [connectBtn setTitle:NSLocalizedString(@"Connect", nil) forState:UIControlStateNormal];
    [self setConnectButtonColor];
    [connectBtn addTarget:self action:@selector(connect:) forControlEvents:UIControlEventTouchUpInside];
    _connectButton = connectBtn;
    [self.view addSubview:_connectButton];
}

- (void)connect:(UIButton *)btn{
    if(_selectedIndexPath){
        SRConnectController *connectVC = [[SRConnectController alloc]initWithNibName:@"SRConnectController" bundle:nil];
        WifiNetwork *network = _networks[_selectedIndexPath.row];
        connectVC.wifiNetwork = network;
        [self.navigationController pushViewController:connectVC animated:YES];
        
    }else{
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"connect_enable", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Confrim", nil) style:UIAlertActionStyleDefault handler:nil];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

- (void)refreshNetWorks{
    [_networks removeAllObjects];
    _selectedIndexPath = nil;
    [self setConnectButtonColor];
    [self.tableView reloadData];
    [self reGetNetworks];
}

- (void)setConnectButtonColor{
    if(_selectedIndexPath){
        [_connectButton setTitleColor:[UIColor colorWithRed:255/255.0 green:150/255.0 blue:1/255.0 alpha:1.0] forState:UIControlStateNormal];
    }else{
        [_connectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)reGetNetworks{
    if(self.isProgressViewShow){
        return;
    }
    self.isProgressViewShow = YES;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[LeadLampManager sharedInstance] searchWifiNetworksNearby:^(NSArray<WifiNetwork *> *networks) {
            _networks = [NSMutableArray arrayWithArray:networks];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                self.isProgressViewShow = NO;
                [self.tableView reloadData];
                _selectedIndexPath = nil;
            });
        }];
    });
}

#pragma mark -- tableview delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]){
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _networks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SRNetworkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"netCell"];
    WifiNetwork *network = _networks[indexPath.row];
    if([_selectedIndexPath isEqual:indexPath]){
        cell.contentView.backgroundColor = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:1/255.0 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor blackColor];
    }
    [cell setupCellInfo:network];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc]init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    headView.backgroundColor = [UIColor clearColor];
    return headView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_selectedIndexPath != indexPath){
        if(_selectedIndexPath){
            SRNetworkCell *foreCell = [tableView cellForRowAtIndexPath:_selectedIndexPath];
            foreCell.contentView.backgroundColor = [UIColor blackColor];
        }
        SRNetworkCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:1/255.0 alpha:1.0];
        _selectedIndexPath = indexPath;
        [self setConnectButtonColor];

    }
}

@end
