//
//  SRLampSettingModel.h
//  RealColor
//
//  Created by sunhong on 2017/3/30.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  LeadLamp;

@interface SRLampSettingModel : NSObject

@property (nonatomic, strong) LeadLamp *lamp;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isOn;

@end
