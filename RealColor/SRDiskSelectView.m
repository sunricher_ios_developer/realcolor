//
//  SRDiskSelectView.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRDiskSelectView.h"

@implementation SRDiskSelectView

+ (instancetype)loadDiskSelectView{
    return [[[NSBundle mainBundle]loadNibNamed:@"SRDiskSelectView" owner:nil options:nil] firstObject];
}

- (IBAction)rgbSelect:(UIButton *)sender {
    if(_selectTypeBlock){
        _selectTypeBlock(SRLEDColorTypeRGB);
        [self removeFromSuperview];
    }
}

- (IBAction)cctSelect:(UIButton *)sender {
    if(_selectTypeBlock){
        _selectTypeBlock(SRLEDColorTypeCCT);
        [self removeFromSuperview];
    }

}
- (IBAction)dimSelect:(UIButton *)sender {
    if(_selectTypeBlock){
        _selectTypeBlock(SRLEDColorTypeDIM);
        [self removeFromSuperview];
    }

}

- (IBAction)backgroundBtn:(UIButton *)sender {
    if(_viewDidRemoveBlock){
        _viewDidRemoveBlock();
    }
    [self removeFromSuperview];
}

@end
