//
//  SRNetworkCell.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRNetworkCell.h"
#import "LeadLampManager.h"

@interface SRNetworkCell ()

@property (weak, nonatomic) IBOutlet UILabel *wifiName;
@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;

@end

@implementation SRNetworkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellInfo:(WifiNetwork *)network{
    if(network){
        if(network.ssid.length > 0){
            self.wifiName.text = network.ssid;
        }else{
            self.wifiName.text = NSLocalizedString(@"unknow", nil);
        }
        
        if ([network.securityType rangeOfString:@"OPEN"].length > 0 || [network.securityType rangeOfString:@"NONE"].length > 0) {
            self.lockImageView.hidden = YES;
        }else{
            self.lockImageView.hidden = NO;
        }

    }
}

@end
