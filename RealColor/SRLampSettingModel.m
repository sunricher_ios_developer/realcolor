//
//  SRLampSettingModel.m
//  RealColor
//
//  Created by sunhong on 2017/3/30.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRLampSettingModel.h"
#import "LeadLampManager.h"

@implementation SRLampSettingModel

- (instancetype)init{
    if(self = [super init]){
        _lamp = [[LeadLamp alloc]init];
    }
    return self;
}

@end
