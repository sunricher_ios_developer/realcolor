//
//  SRFavoriteCell.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRFavoriteCell.h"

@interface SRFavoriteCell ()<UITextFieldDelegate>

@end

@implementation SRFavoriteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _sceneTF.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(_scneNameBlock){
        _scneNameBlock(self,textField.text);
    }
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(_scenNameTFBeginEditBlock){
        _scenNameTFBeginEditBlock();
    }
}

- (IBAction)save:(UIButton *)sender {
    if(_saveBlock){
        _saveBlock();
    }
}

- (IBAction)trigger:(UIButton *)sender {
    if(_triggerBlock){
        _triggerBlock();
    }
}


@end
