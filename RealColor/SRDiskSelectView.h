//
//  SRDiskSelectView.h
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SRLEDColorTypeRGB,
    SRLEDColorTypeCCT,
    SRLEDColorTypeDIM
}SRLEDColorType;

@interface SRDiskSelectView : UIView

+ (instancetype)loadDiskSelectView;
@property (nonatomic, copy) void(^selectTypeBlock)(SRLEDColorType type);
@property (nonatomic, copy) void(^viewDidRemoveBlock)();

@end
