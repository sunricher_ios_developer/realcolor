//
//  SRVerSliderView.h
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRVerSliderView : UIView

- (instancetype)initWithFrame:(CGRect)frame andTransValue:(CGFloat)value;
@property (nonatomic, copy) void(^sliderValueChange)(CGFloat value);

@end
