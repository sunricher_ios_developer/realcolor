//
//  SRDataBaseManger.m
//  RealColor
//
//  Created by sunhong on 2017/3/30.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRDataBaseManger.h"
#import <sqlite3.h>
#import "LeadLampManager.h"
#import "SRLampSettingModel.h"

NSString *const ScenTableName = @"scen_table";
NSString *const LampTableName = @"lamp_table";

@interface SRDataBaseManger ()
{
    sqlite3 *db;
}

@end

@implementation SRDataBaseManger

+ (instancetype)sharedInstance{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SRDataBaseManger alloc]init];
    });
    return instance;
}

- (instancetype)init{
    if(self = [super init]){

        [self open];
        if(![self isTableExist:ScenTableName]){
            NSString *scenSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (scen_index integer primary key autoincrement not null,scen_name text)", ScenTableName];
            char *err;
            int result = sqlite3_exec(db, scenSql.UTF8String, NULL, NULL, &err);
            if (result == SQLITE_OK) {
                [self setupScenTableData];
            }

        }
        if(![self isTableExist:LampTableName]){
            NSString *lampsSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(lampName text,lampMac text primary key,lampIP text,lampIsOn integer,LampIsSelect integer)",LampTableName];
            char *error = NULL;
            int lampRes = sqlite3_exec(db, [lampsSql UTF8String], NULL, NULL, &error);
            
            if (lampRes != SQLITE_OK) {
                NSLog(@"Create %@ failure, err %s", LampTableName, error);
            }
        }
        
        [self close];

    }
    return self;
}

- (BOOL)isTableExist:(NSString *)aTableName
{
    BOOL exist = NO;
    sqlite3_stmt *stmt;
    
    NSString *judgeString = [NSString stringWithFormat:@"SELECT name FROM sqlite_master where type ='table' and name = '%@';",aTableName];
    const char *sql_stmt = [judgeString UTF8String];
    
    if (sqlite3_prepare(db, sql_stmt, -1, &stmt, nil) == SQLITE_OK){
        int temp = sqlite3_step(stmt);
        if (temp == SQLITE_ROW){
            exist = YES;
        } else{
            NSLog(@"temp = %d",temp);
        }
    }
    
    sqlite3_finalize(stmt);
    
    return exist;
}

- (void)setupScenTableData{
    for(int i = 0 ;i < 8; i ++){
        NSString *scenName = [NSString stringWithFormat:NSLocalizedString(@"Scene_name", nil),i + 1];
        [self addScenInfo:scenName index:i + 1];
    }
}

- (void)addScenInfo:(NSString *)scenName index:(NSInteger)scenIndex{
    NSString *sqlite = [NSString stringWithFormat:@"insert into %@(scen_index,scen_name) values ('%d','%@')",ScenTableName,scenIndex,scenName];
    char *error = NULL;
    int result = sqlite3_exec(db, [sqlite UTF8String], nil, nil, &error);
    if (result == SQLITE_OK) {
       // NSLog(@"insert scen success");
    } else {
        NSLog(@"scen error----%s",error);
    }

}

- (void)updateScenInfo:(NSString *)sceneName index:(NSInteger)index{
    [self open];
    NSString *sqlite = [NSString stringWithFormat:@"update %@ set scen_name = '%@' where scen_index = '%d'",ScenTableName,sceneName,index];
    //2.执行sqlite语句
    char *error = NULL;
    int result = sqlite3_exec(db, [sqlite UTF8String], nil, nil, &error);
    if (result != SQLITE_OK) {
        NSLog(@"update scene err!");
    }
    [self close];
}

- (NSDictionary *)getSceneInfo{
    [self open];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSString *sqlite = [NSString stringWithFormat:@"select * from %@",ScenTableName];
    sqlite3_stmt *stmt = NULL;
    
    int result = sqlite3_prepare(db, sqlite.UTF8String, -1, &stmt, NULL);
    if (result == SQLITE_OK) {
        
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            NSInteger index = sqlite3_column_int(stmt, 0);
            NSString *name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 1)] ;
  
            [dic setObject:name forKey:@(index)];
        }
    }
    sqlite3_finalize(stmt);
    [self close];
    return dic;
}

- (void)insertLampSettingInfo:(NSArray *)array{
    [self open];
    for (SRLampSettingModel *model in array) {
        [self insertLampSettingModel:model];
    }
    [self close];
}

- (void)insertLampSettingModel:(SRLampSettingModel *)lampModel{
    [self open];
    if(lampModel){
        NSString *sqlite = [NSString stringWithFormat:@"insert into %@(lampName,lampMac,lampIP,lampIsOn,LampIsSelect) values ('%@','%@','%@','%d','%d')",LampTableName,lampModel.lamp.name,lampModel.lamp.mac,lampModel.lamp.ip,lampModel.isOn,lampModel.isSelected];
        
        char *error = NULL;
        int result = sqlite3_exec(db, [sqlite UTF8String], nil, nil, &error);
        if (result == SQLITE_OK) {
            
        } else {
            NSLog(@"model error----%s,%d",error,result);
        }

    }
    [self close];
}

- (void)updateLampSettingModel:(SRLampSettingModel *)lampModel{
    [self open];
    NSString *sqlite = [NSString stringWithFormat:@"update %@ set lampName = '%@' ,lampIP = '%@',lampIsOn = '%d',LampIsSelect = '%d' where lampMac = '%@'",LampTableName,lampModel.lamp.name,lampModel.lamp.ip,lampModel.isOn,lampModel.isSelected,lampModel.lamp.mac];
    //2.执行sqlite语句
    char *error = NULL;
    int result = sqlite3_exec(db, [sqlite UTF8String], nil, nil, &error);
    if (result != SQLITE_OK) {
        NSLog(@"update lamp err!");
    }
    [self close];
}

- (NSArray<SRLampSettingModel *>*)getlampsArray{
    [self open];
    NSMutableArray *array = [[NSMutableArray alloc] init];

    NSString *sqlite = [NSString stringWithFormat:@"select * from lamp_table"];
    sqlite3_stmt *stmt = NULL;

    int result = sqlite3_prepare(db, sqlite.UTF8String, -1, &stmt, NULL);
    if (result == SQLITE_OK) {
   
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            SRLampSettingModel *model = [[SRLampSettingModel alloc] init];

            model.lamp.name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 0)] ;
            model.lamp.mac = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 1)] ;
            model.lamp.ip = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 2)] ;
            model.isOn = sqlite3_column_int(stmt, 3);
            model.isSelected = sqlite3_column_int(stmt, 4);
            
            [array addObject:model];
        }
    } 
    sqlite3_finalize(stmt);
    [self close];
    return array;
}

- (SRLampSettingModel *)searchModelWith:(NSString *)mac ip:(NSString *)ip{
    [self open];
    NSString *sqlite = [NSString stringWithFormat:@"select lampName,lampIsOn,LampIsSelect from %@ where lampMac = '%@'",LampTableName,mac];
    //2.伴随指针
    sqlite3_stmt *stmt = NULL;
    SRLampSettingModel *model = nil;
    //3.预执行sqlite语句
    int result = sqlite3_prepare(db, sqlite.UTF8String, -1, &stmt, NULL);//第4个参数是一次性返回所有的参数,就用-1
    if (result == SQLITE_OK) {
        while (sqlite3_step(stmt)==SQLITE_ROW){
            model = [[SRLampSettingModel alloc]init];
            model.lamp.name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(stmt, 0)] ;
            model.isOn = sqlite3_column_int(stmt, 1);
            model.isSelected = sqlite3_column_int(stmt, 2);
            model.lamp.ip = ip;
        }
    }
    
    [self close];
    return model;

}

- (NSArray <LeadLamp *>*)selectedLampsArray{
    [self open];
    NSMutableArray *array = [NSMutableArray array];
    NSArray *lamps = [[LeadLampManager sharedInstance] connectedLeadLamps];
    for (LeadLamp *lamp in lamps) {
        SRLampSettingModel *model = [self searchModelWith:lamp.mac ip:lamp.ip];
        if(model){
            if(model.isSelected){
                [array addObject:lamp];
            }
        }else{
            [array addObject:lamp];
        }
    }
    [self close];
    return array;
}

- (void)open{
    int result = sqlite3_open([self dbPath], &db);
    if(result == SQLITE_OK){
        //NSLog(@"dataBase open success!");
    }
}

- (void)close {
    sqlite3_close(db);
}

- (const char *)dbPath {
    NSString *doc = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *path = [doc stringByAppendingPathComponent:@"realColor_dataBase"];
    return path.UTF8String;
}


@end
