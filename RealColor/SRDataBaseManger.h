//
//  SRDataBaseManger.h
//  RealColor
//
//  Created by sunhong on 2017/3/30.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SRLampSettingModel;
@class LeadLamp;

@interface SRDataBaseManger : NSObject

+ (instancetype) sharedInstance;
- (NSArray<SRLampSettingModel *>*)getlampsArray;
- (void)updateScenInfo:(NSString *)sceneName index:(NSInteger)index;
- (NSDictionary *)getSceneInfo;
- (void)insertLampSettingModel:(SRLampSettingModel *)lampModel;
- (void)updateLampSettingModel:(SRLampSettingModel *)lampModel;
- (void)insertLampSettingInfo:(NSArray *)array;
- (SRLampSettingModel *)searchModelWith:(NSString *)mac ip:(NSString *)ip;
- (NSArray <LeadLamp *>*)selectedLampsArray;

@end
