//
//  SRVerSliderView.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRVerSliderView.h"

@interface SRVerSliderView()

@property (nonatomic, strong) UISlider *slider;

@end

@implementation SRVerSliderView

- (instancetype)initWithFrame:(CGRect)frame andTransValue:(CGFloat)value{
    if(self = [super initWithFrame:frame]){
        
        UIImageView *runSliderBgView = [[UIImageView alloc]initWithFrame:self.bounds];
        runSliderBgView.image = [UIImage imageNamed:@"bar_background_v"];
        [self addSubview:runSliderBgView];
        
        CGRect sliderFrame = CGRectMake(0, 0, frame.size.height, frame.size.width - 5);
        sliderFrame.origin.x = sliderFrame.origin.x - sliderFrame.size.width / 2 + sliderFrame.size.height / 2 + 3;
        sliderFrame.origin.y = sliderFrame.origin.y + sliderFrame.size.width / 2 - sliderFrame.size.height / 2;
        _slider = [[UISlider alloc]initWithFrame:sliderFrame];
        _slider.minimumValue = 0;
        _slider.maximumValue = 1;
        _slider.value = 1.0;
        [self addSubview:_slider];
        CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * value);
        _slider.transform = trans;
        [_slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
        [_slider setThumbImage:[UIImage imageNamed:@"bar_tap"] forState:UIControlStateNormal];
        [_slider setMinimumTrackImage:[UIImage imageNamed:@"bar_white_bg_h"] forState:UIControlStateNormal];
        [_slider setMaximumTrackImage:[UIImage imageNamed:@"bar_grey_bg_h"] forState:UIControlStateNormal];

    }
    
    return self;
}

- (void)sliderValueChange:(UISlider *)slider{
//    NSLog(@"%f",slider.value);
    if(_sliderValueChange){
        _sliderValueChange(slider.value);
    }
}

@end
