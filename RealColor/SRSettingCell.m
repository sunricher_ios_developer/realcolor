//
//  SRSettingCell.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRSettingCell.h"
#import "SRLampSettingModel.h"
#import "LeadLampManager.h"
#import "SRDataBaseManger.h"

@interface SRSettingCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *ipLable;
@property (weak, nonatomic) IBOutlet UILabel *macLabel;
@property (weak, nonatomic) IBOutlet UIButton *switchLedBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (nonatomic, strong) SRLampSettingModel *lampModel;

@end

@implementation SRSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _nameTextField.tintColor = [UIColor whiteColor];
    _nameTextField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellInfo:(SRLampSettingModel *)LampModel{
    if(LampModel){
        _lampModel = LampModel;
        _ipLable.text = _lampModel.lamp.ip;
        _macLabel.text = _lampModel.lamp.mac;
        if(_lampModel.lamp.name.length > 0){
            _nameTextField.text = _lampModel.lamp.name;
        }else{
            _nameTextField.text = NSLocalizedString(@"RealColor", nil);
        }
        self.selectedBtn.selected = _lampModel.isSelected;
        self.switchLedBtn.selected = _lampModel.isOn;
        if(self.selectedBtn.selected){
            self.switchLedBtn.hidden = NO;
        }else{
            self.switchLedBtn.hidden = YES;
        }
    }
}

- (IBAction)selectWifi:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected){
        self.switchLedBtn.hidden = NO;
    }else{
        self.switchLedBtn.hidden = YES;
    }
    _lampModel.isSelected = sender.selected;
    [[SRDataBaseManger sharedInstance] updateLampSettingModel:_lampModel];
}

- (IBAction)switchLedClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    _lampModel.isOn = sender.selected;
    [[SRDataBaseManger sharedInstance] updateLampSettingModel:_lampModel];
    LeadLampCommand *comd;
    if(sender.selected){
        comd = [LeadLampCommand powerOn];
    }else{
        comd = [LeadLampCommand powerOff];
    }
    [self sendCommd:comd];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(textField.text.length > 0 && ![textField.text isEqualToString:_lampModel.lamp.name]){
        _lampModel.lamp.name = textField.text;
        [[SRDataBaseManger sharedInstance] updateLampSettingModel:_lampModel];
    }
    [textField resignFirstResponder];
    return YES;
}

- (void)sendCommd:(LeadLampCommand *)comd{
    [[LeadLampManager sharedInstance] sendCommand:comd toLamp:_lampModel.lamp completion:nil];
}

@end
