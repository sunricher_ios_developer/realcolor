//
//  SRSwitchViewController.m
//  RealColor
//
//  Created by sunhong on 2017/3/28.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRSwitchViewController.h"

@interface SRSwitchViewController ()

@end

@implementation SRSwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
