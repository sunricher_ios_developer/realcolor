//
//  SRSettingViewController.m
//  RealColor
//
//  Created by sunhong on 2017/3/28.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRSettingViewController.h"
#import "SRNetworksController.h"
#import "SRLampSettingModel.h"
#import "SRDataBaseManger.h"
#import "LeadLampManager.h"
#import "SRSettingCell.h"
#import "MBProgressHUD.h"

@interface SRSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *lampsArray;
@property (nonatomic, strong) NSArray *titleArray;

@end

@implementation SRSettingViewController

- (UITableView *)tableView{
    if(!_tableView){
        CGFloat TopNavHeight = ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 88 : 64);
        CGFloat  BottomHeight =  ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 83 : 49);
        UITableView *tb = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - BottomHeight - TopNavHeight) style:UITableViewStyleGrouped];
        tb.delegate = self;
        tb.dataSource = self;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:tb.bounds];
        imageView.image = [UIImage imageNamed:@"background"];
        tb.backgroundView = imageView;
        tb.separatorColor = [UIColor colorWithRed:65/255.0 green:65/255.0 blue:65/255.0 alpha:1.0];
        _tableView = tb;
        [_tableView registerNib:[UINib nibWithNibName:@"SRSettingCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SRSettingCell"];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSArray *)titleArray{
    if(!_titleArray){
        _titleArray = @[NSLocalizedString(@"setting_cell_title1", nil),NSLocalizedString(@"setting_cell_title2", nil)];
    }
    return _titleArray;
}

- (NSMutableArray *)lampsArray{
    if(!_lampsArray){
        _lampsArray = [NSMutableArray array];
    }
    return _lampsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    [self tableView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshData];
}

- (void)refreshData{
    NSArray *lamps = [[SRDataBaseManger sharedInstance] getlampsArray];
    NSArray *leadLamps = [[LeadLampManager sharedInstance] connectedLeadLamps];
    [self.lampsArray removeAllObjects];
    
    if(leadLamps.count > 0){
        for (LeadLamp *leadLamp in leadLamps) {
            SRLampSettingModel *model = nil;
            if(lamps.count > 0){
                model = [[SRDataBaseManger sharedInstance] searchModelWith:leadLamp.mac ip:leadLamp.ip];
            }
            if(model){
                model.lamp.mac = leadLamp.mac;
                model.lamp.ip = leadLamp.ip;
                [self.lampsArray addObject:model];
            }else{
                SRLampSettingModel *lampModel = [[SRLampSettingModel alloc]init];
                lampModel.lamp = leadLamp;
                lampModel.isOn = YES;
                lampModel.isSelected = YES;
                [self.lampsArray addObject:lampModel];
                [[SRDataBaseManger sharedInstance] insertLampSettingModel:lampModel];
            }
        }
        [self tableViewReloadData];
    }else{
        [self tableViewReloadData];
    }
}

- (void)tableViewReloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


#pragma mark -- tableview delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]){
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.lampsArray.count > 0){
        return 2;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.lampsArray.count > 0){
        if(section == 0){
            return self.lampsArray.count;
        }else{
            return 2;
        }

    }else{
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    if(indexPath.section == 0 && self.lampsArray.count > 0){
        SRSettingCell *wifiCell = [tableView dequeueReusableCellWithIdentifier:@"SRSettingCell"];
        SRLampSettingModel *lamp = self.lampsArray[indexPath.row];
        [wifiCell setupCellInfo:lamp];
        wifiCell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = wifiCell;
    }
    
    if (indexPath.section == 1 || (indexPath.section == 0 && self.lampsArray.count == 0)){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = self.titleArray[indexPath.row];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing:YES];
    __weak typeof(self)weakSelf = self;
    if(indexPath.section == 1 || (self.lampsArray.count == 0 && indexPath.section == 0)){
        if(indexPath.row == 0){
            if(self.lampsArray.count == 0){
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"wifi_not_found", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:nil];
                [alertVC addAction:okAction];
                [self presentViewController:alertVC animated:YES completion:nil];
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"restore_wifi", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [MBProgressHUD showHUDAddedTo:weakSelf.tableView animated:YES];
                    [[LeadLampManager sharedInstance] restoreToFactorySettings:^(NSError *err) {
  
                        SRSettingViewController *strongSelf = weakSelf;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [MBProgressHUD hideHUDForView:strongSelf.tableView animated:YES];
                            MBProgressHUD *hue = [MBProgressHUD showHUDAddedTo:strongSelf.view animated:true];
                            hue.mode = MBProgressHUDModeText;
                            hue.label.numberOfLines = 0;
                            if(err){
                                hue.label.text = NSLocalizedString(@"restore_fail", nil);
                            }else{
                                hue.label.text = NSLocalizedString(@"restore_success", nil);
                            }
                            hue.removeFromSuperViewOnHide = true;
                            [hue hideAnimated:true afterDelay:1.5];
                            [self refreshData];
                        });
                        
                    }];

                }];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:nil];
                [alertVC addAction:okAction];
                [alertVC addAction:cancelAction];
                [self presentViewController:alertVC animated:YES completion:nil];
                
            });
        }
        if(indexPath.row == 1){
            SRNetworksController *netVC = [[SRNetworksController alloc]init];
            [self.navigationController pushViewController:netVC animated:YES];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.tableView endEditing:YES];
}

@end
