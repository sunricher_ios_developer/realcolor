//
//  AppDelegate.m
//  RealColor
//
//  Created by sunhong on 2017/3/28.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "AppDelegate.h"
#import "LeadLampManager.h"
#import "MBProgressHUD.h"
#import "SRLampSettingModel.h"
#import "SRDataBaseManger.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self searchLamps];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [self searchLamps];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)searchLamps {
 
    [MBProgressHUD showHUDAddedTo:_window.rootViewController.view animated:true];
    
    __weak typeof(self) weakSelf = self;
    [[LeadLampManager sharedInstance] searchLeadLamps:^(NSArray<LeadLamp *> *lamps) {
        dispatch_async(dispatch_get_main_queue(), ^{
             [MBProgressHUD hideHUDForView:weakSelf.window.rootViewController.view animated:true];
        });
        if(lamps.count == 0){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"wifi_not_found", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Comfirm", nil) style:UIAlertActionStyleDefault handler:nil];
                [alertVC addAction:okAction];
                [weakSelf.window.rootViewController presentViewController:alertVC animated:YES completion:nil];
            });
        }else{
            for (LeadLamp *leadLamp in lamps) {
                SRLampSettingModel *model = nil;
                model = [[SRDataBaseManger sharedInstance] searchModelWith:leadLamp.mac ip:leadLamp.ip];
                if(!model){
                    SRLampSettingModel *lampModel = [[SRLampSettingModel alloc]init];
                    lampModel.lamp = leadLamp;
                    lampModel.isOn = YES;
                    lampModel.isSelected = YES;
                    [[SRDataBaseManger sharedInstance] insertLampSettingModel:lampModel];
                }
            }

        }
    }];
}

@end
