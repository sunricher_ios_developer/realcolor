//
//  SRRGBChangeView.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRRGBChangeView.h"
#import "LeadLampManager.h"
#import "SRDataBaseManger.h"

@interface SRRGBChangeView ()

@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;

@end

@implementation SRRGBChangeView

+ (instancetype)loadSRRGBChangeView{
    return [[[NSBundle mainBundle]loadNibNamed:@"SRRGBChangeView" owner:self options:nil] firstObject];
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [_redSlider setThumbImage:[UIImage imageNamed:@"bar_tap"] forState:UIControlStateNormal];
    [_redSlider setMinimumTrackImage:[UIImage imageNamed:@"bar_red_bg_h"] forState:UIControlStateNormal];
    [_redSlider setMaximumTrackImage:[UIImage imageNamed:@"bar_grey_bg_h"] forState:UIControlStateNormal];
    
    [_greenSlider setThumbImage:[UIImage imageNamed:@"bar_tap"] forState:UIControlStateNormal];
    [_greenSlider setMinimumTrackImage:[UIImage imageNamed:@"bar_green_bg_h"] forState:UIControlStateNormal];
    [_greenSlider setMaximumTrackImage:[UIImage imageNamed:@"bar_grey_bg_h"] forState:UIControlStateNormal];
    
    [_blueSlider setThumbImage:[UIImage imageNamed:@"bar_tap"] forState:UIControlStateNormal];
    [_blueSlider setMinimumTrackImage:[UIImage imageNamed:@"bar_blue_bg_h"] forState:UIControlStateNormal];
    [_blueSlider setMaximumTrackImage:[UIImage imageNamed:@"bar_grey_bg_h"] forState:UIControlStateNormal];
}

- (IBAction)redChange:(UISlider *)sender {
    self.redLabel.text = [NSString stringWithFormat:@"%d",(int)sender.value];
    LeadLampCommand *comd = [LeadLampCommand rgbRed:sender.value];
    [self sendCommd:comd];
}

- (IBAction)greenChange:(UISlider *)sender {
    self.greenLabel.text = [NSString stringWithFormat:@"%d",(int)sender.value];
    LeadLampCommand *comd = [LeadLampCommand rgbGreen:sender.value];
    [self sendCommd:comd];
}

- (IBAction)blueChange:(UISlider *)sender {
    self.blueLabel.text = [NSString stringWithFormat:@"%d",(int)sender.value];
    LeadLampCommand *comd = [LeadLampCommand rgbBlue:sender.value];
    [self sendCommd:comd];
}

- (void)sendCommd:(LeadLampCommand *)comd{
    [[LeadLampManager sharedInstance] sendCommand:comd toLamps:[[SRDataBaseManger sharedInstance] selectedLampsArray] completion:^(NSError *err) {
        if(err){
            //NSLog(@"comd error:%@",err);
        }
    }];
}

@end
