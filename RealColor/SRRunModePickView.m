//
//  SRRunModePickView.m
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRRunModePickView.h"
#import "SRVerSliderView.h"
#import "LeadLampManager.h"
#import "SRDataBaseManger.h"

NSString *const SelectedRunModel = @"SelectedRunModel";

@interface SRRunModePickView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, strong) NSArray *modeArray;
@property (nonatomic, strong) UIPickerView *pickView;
@property (nonatomic, strong) UIImageView *runSliderBgView;

@end

@implementation SRRunModePickView

- (instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        
        CGFloat pickW = 160 * [UIScreen mainScreen].bounds.size.width / 375.0;
        CGFloat pickH = pickW * 210 / 160.0;
        
        _pickView = [[UIPickerView alloc]initWithFrame:CGRectMake((frame.size.width - pickW) / 2.0, (frame.size.height - pickH )/ 2.0, pickW, pickH)];
        _pickView.delegate =self;
        _pickView.dataSource = self;
        [self addSubview:_pickView];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *selectedMode = [defaults objectForKey:SelectedRunModel];
        if(SelectedRunModel){
            [_pickView selectRow:([selectedMode intValue] - 1) inComponent:0 animated:YES];
        }
        
        SRVerSliderView *sliderView = [[SRVerSliderView alloc]initWithFrame:CGRectMake(16, 0, 64, frame.size.height) andTransValue:0.5];
        [sliderView setSliderValueChange:^(CGFloat value) {
            LeadLampCommand *commd = [LeadLampCommand rgbRunSpeed:value];
            [self sendCommd:commd];
        }];
        [self addSubview:sliderView];
    }
    return self;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 10;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat:@"Mode %d",row + 1];
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSAttributedString *str = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"Mode %d",row + 1] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return str;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@(row + 1) forKey:SelectedRunModel];
    [defaults synchronize];
    
    LeadLampCommand *comd = [LeadLampCommand rgbStartRun:row + 1];
    [self sendCommd:comd];
}

- (void)sendCommd:(LeadLampCommand *)comd{
    [[LeadLampManager sharedInstance] sendCommand:comd toLamps:[[SRDataBaseManger sharedInstance] selectedLampsArray] completion:nil];
}

@end
