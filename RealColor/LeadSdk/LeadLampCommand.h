//
//  LeadLampCommand.h
//  LeadLedDemo
//
//  Created by wangwendong on 2017/3/24.
//  Copyright © 2017年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeadLampCommand : NSObject

///
@property (strong, nonatomic) NSArray<NSData *> *datas;
- (NSData *)dataBySelf;

+ (LeadLampCommand *)powerOn;
+ (LeadLampCommand *)powerOff;

+ (LeadLampCommand *)rgbHue:(CGFloat)hue;
+ (LeadLampCommand *)rgbWhite:(CGFloat)white;
+ (LeadLampCommand *)rgbBrightness:(CGFloat)brightness;
+ (LeadLampCommand *)rgbRed:(CGFloat)red;
+ (LeadLampCommand *)rgbGreen:(CGFloat)green;
+ (LeadLampCommand *)rgbBlue:(CGFloat)blue;
+ (LeadLampCommand *)rgbStartRun:(UInt8)mode;
+ (LeadLampCommand *)rgbRunSpeed:(CGFloat)speed;

+ (LeadLampCommand *)cctHue:(CGFloat)hue;
+ (LeadLampCommand *)cctBrightness:(CGFloat)brightness;

+ (LeadLampCommand *)dimHue:(CGFloat)hue;

+ (LeadLampCommand *)saveSceneAtIndex:(UInt8)index;
+ (LeadLampCommand *)triggerSceneAtIndex:(UInt8)index;

@end
