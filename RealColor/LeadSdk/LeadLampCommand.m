//
//  LeadLampCommand.m
//  LeadLedDemo
//
//  Created by wangwendong on 2017/3/24.
//  Copyright © 2017年 Sunricher. All rights reserved.
//

#import "LeadLampCommand.h"

@interface LeadLampCommand ()

@property (nonatomic) Byte head_0;

@property (nonatomic) Byte appIdFirst_1;
@property (nonatomic) Byte appIdSecond_2;
@property (nonatomic) Byte appIdLast_3;

@property (nonatomic) Byte deviceType_4;

@property (nonatomic) Byte selectedArea_5;

@property (nonatomic) Byte dataType_6;
@property (nonatomic) Byte key_7;
@property (nonatomic) Byte value_8;

//@property (nonatomic) Byte sum_9;

@property (nonatomic) Byte endFirst_10;
@property (nonatomic) Byte endLast_11;

@end

@implementation LeadLampCommand

- (instancetype)init {
    self = [super init];
    if (self) {
        self.head_0 = 0x55;
        
        self.appIdFirst_1 = 0x00;
        self.appIdSecond_2 = 0x00;
        self.appIdLast_3 = 0x01;
        self.selectedArea_5 = 0x01;
        
        self.deviceType_4 = 0x02;
        
        self.endFirst_10 = 0xAA;
        self.endLast_11 = 0xAA;
    }
    return self;
}

+ (LeadLampCommand *)powerOn {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x02;
    cmd.key_7 = 0x12;
    cmd.value_8 = 0xAB;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)powerOff {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x02;
    cmd.key_7 = 0x12;
    cmd.value_8 = 0xA9;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbHue:(CGFloat)hue {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x01;
    cmd.key_7 = 0x01;
    // [0x01, 0x60]
    int value = 0x61 - (round(hue * 0x60)) + 43;
    value = (value % 0x60);
    if (value == 0) {
        value = 0x60;
    }
    cmd.value_8 = value;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbWhite:(CGFloat)white {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x4B;
    cmd.value_8 = (Byte)round(white * 0xFF);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbBrightness:(CGFloat)brightness {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x4C;
    // 0x01 ~ 0x40
    int value = round(brightness * 0x40);
    if (value <= 0) {
        value = 1;
    }
    cmd.value_8 = (Byte)value;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbRed:(CGFloat)red {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x48;
    cmd.value_8 = (Byte)round(red * 0xFF);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbGreen:(CGFloat)green {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x49;
    cmd.value_8 = (Byte)round(green * 0xFF);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbBlue:(CGFloat)blue {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x4A;
    cmd.value_8 = (Byte)round(blue * 0xFF);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbStartRun:(UInt8)mode {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x02;
    cmd.key_7 = 0x4E;
    // [0x15, 0x1E]
    int value = mode + 0x14;
    if (value < 0x15) {
        value = 0x15;
    } else if (value > 0x1E) {
        value = 0x1E;
    }
    cmd.value_8 = (Byte)value;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)rgbRunSpeed:(CGFloat)speed {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x22;
    // [0x01, 0x0A]
    cmd.value_8 = (Byte)round(speed * 0x0A);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)cctHue:(CGFloat)hue {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x36;
    // [0x00, 0x20]
    cmd.value_8 = (Byte)round(hue * 0x20);
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)cctBrightness:(CGFloat)brightness {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x33;
    // [0x00, 0xFF]
    int value = round(brightness * 0xFF);
    if (value <= 0) {
        value = 1;
    }
    cmd.value_8 = (Byte)value;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)dimHue:(CGFloat)hue {
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x08;
    cmd.key_7 = 0x38;
    // [0x00, 0xFF]
    int value = round(hue * 0xFF);
    if (value <= 0) {
        value = 1;
    }
    cmd.value_8 = (Byte)value;
    
    cmd.datas = @[[cmd dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)saveSceneAtIndex:(UInt8)index {
    // Start
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x02;
    cmd.key_7 = 0x14;
    cmd.value_8 = 0xB1;
    
    // Save
    LeadLampCommand *cmd1 = [[LeadLampCommand alloc] init];
    
    index = index - 1;
    if (index < 0) {
        index = 0;
    } else if (index > 7) {
        index = 7;
    }
    
    cmd1.dataType_6 = 0x02;
    cmd1.key_7 = 0x0A + index;
    cmd1.value_8 = 0x91 + (3 * index);
    
    cmd.datas = @[[cmd dataBySelf], [cmd1 dataBySelf]];
    return cmd;
}

+ (LeadLampCommand *)triggerSceneAtIndex:(UInt8)index {
    // Start
    LeadLampCommand *cmd = [[LeadLampCommand alloc] init];
    
    cmd.dataType_6 = 0x02;
    cmd.key_7 = 0x14;
    cmd.value_8 = 0xB0;
    
    // Trigger
    LeadLampCommand *cmd1 = [[LeadLampCommand alloc] init];
    
    index = index - 1;
    if (index < 0) {
        index = 0;
    } else if (index > 7) {
        index = 7;
    }
    
    cmd1.dataType_6 = 0x02;
    cmd1.key_7 = 0x0A + index;
    cmd1.value_8 = 0x91 + (3 * index);
    
    cmd.datas = @[[cmd dataBySelf], [cmd1 dataBySelf]];
    return cmd;
}

#pragma mark - Private 

- (Byte)sum_9 {
    Byte sum = (Byte)(_deviceType_4 + _selectedArea_5 + _dataType_6 + _key_7 + _value_8);
    
    return sum;
}

- (NSData *)dataBySelf {
    Byte sum_9 = [self sum_9];
    
    const Byte bytes[] = {_head_0, _appIdFirst_1, _appIdSecond_2, _appIdLast_3, _deviceType_4, _selectedArea_5, _dataType_6, _key_7, _value_8, sum_9, _endFirst_10, _endLast_11};
    
    NSData *data = [NSData dataWithBytes:bytes length:12];
    return data.copy;
}

@end
