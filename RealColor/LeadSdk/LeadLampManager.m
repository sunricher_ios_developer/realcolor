//
//  LeadLampManager.m
//  LeadLedDemo
//
//  Created by wangwendong on 2017/3/23.
//  Copyright © 2017年 Sunricher. All rights reserved.
//

#import "LeadLampManager.h"
#import "GCDAsyncSocket.h"
#import "GCDAsyncUdpSocket.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <SystemConfiguration/CaptiveNetwork.h>

const uint16_t SR_UDP_PORT = 48899;
const uint16_t SR_TCP_PORT = 8899;

NSString *const SR_UDP_CMD_HF = @"HF-A11ASSISTHREAD";
NSString *const SR_UDP_CMD_OK = @"+ok";
NSString *const SR_UDP_CMD_SCAN = @"AT+WSCAN\r";
NSString *const SR_UDP_CMD_RELD = @"AT+RELD\r";


typedef NS_ENUM(long, SRUDPTag) {
    SRUDPTag_Nothing = 1 << 0,
    SRUDPTag_SearchLeadLamps = 1 << 1,
    SRUDPTag_SearchWifiNetworksNearby = 1 << 2,
    SRUDPTag_ConnectToNetwork = 1 << 3,
    SRUDPTag_RestoreToFactorySettings = 1 << 4
};

#pragma mark - LeadLamp

@implementation LeadLamp

- (instancetype)init {
    self = [super init];
    if (self) {
        self.mac = @"";
        self.ip = @"";
        self.name = @"Lead lamp";
    }
    return self;
}

@end

#pragma mark - WifiNetwork

@implementation WifiNetwork

- (instancetype)init {
    self = [super init];
    if (self) {
        self.channel = @"1";
        self.ssid = @"Unknown";
        self.macAddress = @"";
        self.securityType = @"Unknown";
        self.signal = @"0";
    }
    return self;
}

@end

#pragma mark - LeadLampManager

@interface LeadLampManager () <GCDAsyncSocketDelegate, GCDAsyncUdpSocketDelegate> {
    dispatch_queue_t managerSerialQueue;
    dispatch_queue_t managerConcurrentQueue;
    dispatch_queue_t mainQueue;
}

/// <IP, LeadLamp>
@property (strong, nonatomic) NSMutableDictionary<NSString *, LeadLamp *> *foundLampsDictionary;
/// <IP, GCDAsyncSocket>
@property (strong, nonatomic) NSMutableDictionary<NSString *, GCDAsyncSocket *> *connectedSocketsDictionary;
/// <Mac, WifiNetwork>
@property (strong, nonatomic) NSMutableDictionary<NSString *, WifiNetwork *> *nearybyNetworksDictionary;


@property (strong, nonatomic) NSString *localIP;
@property (strong, nonatomic) NSString *udpIP;

@property (strong, nonatomic) GCDAsyncUdpSocket *udpSocket;
@property (nonatomic) SRUDPTag udpTag;

@property (strong, nonatomic) NSTimer *heartBeatTimer;

@end

@implementation LeadLampManager

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[LeadLampManager alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        managerSerialQueue = dispatch_queue_create("managerSerialQueue", DISPATCH_QUEUE_SERIAL);
        managerConcurrentQueue = dispatch_queue_create("maangerConcurrentQueue", DISPATCH_QUEUE_CONCURRENT);
        mainQueue = dispatch_get_main_queue();
        
        _foundLampsDictionary = [NSMutableDictionary dictionary];
        _connectedSocketsDictionary = [NSMutableDictionary dictionary];
        _nearybyNetworksDictionary = [NSMutableDictionary dictionary];
        
        _udpTag = SRUDPTag_Nothing;
        
        [self startHeartBeat];
    }
    return self;
}

- (NSArray<LeadLamp *> *)connectedLeadLamps {
    NSMutableArray *lamps = [NSMutableArray array];
    for (NSString *ip in _connectedSocketsDictionary.allKeys) {
        LeadLamp *lamp = _foundLampsDictionary[ip];
        if (lamp && ![lamps containsObject:lamp]) {
            [lamps addObject:lamp];
        }
    }
    return lamps.copy;
}

- (void)searchLeadLamps:(void (^)(NSArray<LeadLamp *> *))completion {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerSerialQueue, ^{
        if (weakSelf.udpTag == SRUDPTag_SearchLeadLamps) {
            NSLog(@"Searching");
            return;
        }
        
        [weakSelf disconnectAllSockets];
        
        if (![weakSelf resetUdp]) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion(@[]);
                });
            }
            weakSelf.udpTag = SRUDPTag_Nothing;
            return;
        }
        
        [weakSelf sendUdp:SR_UDP_CMD_HF times:30 tag:SRUDPTag_SearchLeadLamps];
        
        if (weakSelf.foundLampsDictionary.allKeys.count > 0) {
            // Connect them
            for (NSString *ip in weakSelf.foundLampsDictionary.allKeys) {
                GCDAsyncSocket *sock = weakSelf.connectedSocketsDictionary[ip];
                NSString *actualIP = [weakSelf actualIPv4OrIPv6WithIP:ip port:SR_TCP_PORT];
                if (!sock) {
                    sock = [[GCDAsyncSocket alloc] initWithDelegate:weakSelf delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
                    NSError *err = nil;
                    [sock connectToHost:actualIP onPort:SR_TCP_PORT error:&err];
                    if (err) {
                        NSLog(@"connectToHost err: %@", err);
                    } else {
                        [weakSelf.connectedSocketsDictionary setObject:sock forKey:ip];
                    }
                } else {
                    if (![sock isConnected]) {
                        NSError *err = nil;
                        sock.delegate = weakSelf;
                        [sock connectToHost:actualIP onPort:SR_TCP_PORT error:&err];
                        if (err) {
                            NSLog(@"connectToHost err: %@", err);
                        } else {
                            [weakSelf.connectedSocketsDictionary setObject:sock forKey:ip];
                        }
                    }
                }
            }
            
            [NSThread sleepForTimeInterval:0.2 * weakSelf.foundLampsDictionary.allKeys.count];
            
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion([weakSelf connectedLeadLamps]);
                });
            }
            weakSelf.udpTag = SRUDPTag_Nothing;
        } else {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion(@[]);
                });
            }
            weakSelf.udpTag = SRUDPTag_Nothing;
        }
        
    });
}

- (BOOL)isConnectedLeadLampDirectly {
    if (_localIP && [_localIP rangeOfString:@"10.10.100"].length > 0) {
        return true;
    }
    return false;
}

- (void)searchWifiNetworksNearby:(void (^)(NSArray<WifiNetwork *> *))completion {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerSerialQueue, ^{
        if (_udpTag == SRUDPTag_SearchWifiNetworksNearby) {
            NSLog(@"search Wifi networks ing");
            return;
        }
        
//        [weakSelf disconnectAllSockets];
        
        [_nearybyNetworksDictionary removeAllObjects];
        
        if (![weakSelf resetUdp]) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion(@[]);
                });
            }
            weakSelf.udpTag = SRUDPTag_Nothing;
            return;
        }
        
        [weakSelf sendUdp:SR_UDP_CMD_HF times:30 tag:SRUDPTag_SearchWifiNetworksNearby];
        
        [weakSelf sendUdp:SR_UDP_CMD_OK times:3 delay:0.2 tag:SRUDPTag_SearchWifiNetworksNearby];
        
        [weakSelf sendUdp:SR_UDP_CMD_SCAN times:3 delay:0.2 tag:SRUDPTag_SearchWifiNetworksNearby];
        
        [NSThread sleepForTimeInterval:6];
        
        if (completion) {
            dispatch_async(mainQueue, ^{
                completion(weakSelf.nearybyNetworksDictionary.allValues);
            });
        }
        weakSelf.udpTag = SRUDPTag_Nothing;
    });
}

- (void)connectToTheWifiNetwork:(WifiNetwork *)network passwork:(NSString *)password completed:(void (^)(NSError *))completion {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerSerialQueue, ^{
        if (!network || !network.ssid) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    NSError *err = [NSError errorWithDomain:NSMachErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"network is nil or ssid is nil."}];
                    completion(err);
                });
            }
            weakSelf.udpTag = SRUDPTag_Nothing;
            return;
        }
        
        NSLog(@"Start connect to %@ and password is %@", network.ssid, password);
        
        [weakSelf disconnectAllSockets];
        
        if (![weakSelf resetUdp]) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion([NSError errorWithDomain:NSMachErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"reset udp err"}]);
                });
            }
            
            weakSelf.udpTag = SRUDPTag_Nothing;
            return;
        }
        
        NSString *nameForSend = [NSString stringWithFormat:@"AT+WSSSID=%@\r", network.ssid];
        
        NSString *passwordForSend = @"";
        if (network.securityType) {
            NSString *xxxx = @"";
            NSString *yyyy = @"";
            NSString *zzzz = password ? password : @"";
            
            NSArray *securityComps = [network.securityType componentsSeparatedByString:@"/"];
            if (securityComps.count != 2) {
                if (completion) {
                    dispatch_async(mainQueue, ^{
                        completion([NSError errorWithDomain:NSMachErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"network's security could not distinguish"}]);
                    });
                }
                _udpTag = SRUDPTag_Nothing;
                return;
            }
            
            NSString *first = [securityComps.firstObject uppercaseString];
            NSString *last = [securityComps.lastObject uppercaseString];
            
            if ([first rangeOfString:@"OPEN"].length > 0 || [first rangeOfString:@"NONE"].length > 0) {
                xxxx = @"OPEN";
                yyyy = @"NONE";
            } else if ([first rangeOfString:@"SHARED"].length > 0) {
                xxxx = @"SHARED";
                yyyy = @"WEP-H";
            } else if ([first rangeOfString:@"WPAPSK"].length > 0 || [first rangeOfString:@"WPA1PSK"].length > 0) {
                xxxx = @"WPAPSK";
                yyyy = @"TKIP";
            }
            
            if ([first rangeOfString:@"WPA2PSK"].length > 0) {
                xxxx = @"WPA2PSK";
                yyyy = @"AES";
            }
            
            if ([last rangeOfString:@"AES"].length > 0) {
                yyyy = @"AES";
            }
            
            passwordForSend = [NSString stringWithFormat:@"AT+WSKEY=%@,%@,%@\r", xxxx, yyyy, zzzz];
            
        } else {
            passwordForSend = @"AT+WSKEY=OPEN,NONE,\r";
        }
        
        NSString *modeForSend = @"AT+WMODE=STA\r";
        
        NSString *atzForSend = @"AT+Z\r";
        
        NSLog(@"Connect to home network with:\n[%@]\n[%@]\n[%@]\n[%@]\n", nameForSend, passwordForSend, modeForSend, atzForSend);
        
        [weakSelf sendUdp:nameForSend times:3 tag:SRUDPTag_ConnectToNetwork];
        
        [weakSelf sendUdp:passwordForSend times:3 delay:0.2 tag:SRUDPTag_ConnectToNetwork];
        
        [weakSelf sendUdp:modeForSend times:3 delay:0.2 tag:SRUDPTag_ConnectToNetwork];
        
        [weakSelf sendUdp:atzForSend times:3 delay:0.2 tag:SRUDPTag_ConnectToNetwork];
        
        if (completion) {
            dispatch_async(mainQueue, ^{
                completion(nil);
            });
        }
        weakSelf.udpTag = SRUDPTag_Nothing;
    });
}

- (void)restoreToFactorySettings:(void (^)(NSError *))completion {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerSerialQueue, ^{
        [weakSelf disconnectAllSockets];
        
        if (![weakSelf resetUdp]) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    completion([NSError errorWithDomain:NSMachErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"reset udp err"}]);
                });
            }
            
            weakSelf.udpTag = SRUDPTag_Nothing;
            return;
        }
        
        [weakSelf sendUdp:SR_UDP_CMD_HF times:30 tag:SRUDPTag_RestoreToFactorySettings];
        
        [weakSelf sendUdp:SR_UDP_CMD_OK times:3 delay:.2 tag:SRUDPTag_RestoreToFactorySettings];
        
        [weakSelf sendUdp:SR_UDP_CMD_RELD times:3 delay:.2 tag:SRUDPTag_RestoreToFactorySettings];
        
        if (completion) {
            dispatch_async(mainQueue, ^{
                completion(nil);
            });
        }
        weakSelf.udpTag = SRUDPTag_Nothing;
    });
}

- (void)sendCommand:(LeadLampCommand *)cmd toLamp:(LeadLamp *)lamp completion:(void (^)(NSError *err))completion {
    [self sendCommand:cmd toLamps:@[lamp] completion:completion];
}

- (void)sendCommand:(LeadLampCommand *)cmd toLamps:(NSArray<LeadLamp *> *)lamps completion:(void (^)(NSError *err))completion {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerConcurrentQueue, ^{
        if (!lamps || lamps.count < 1) {
            if (completion) {
                dispatch_async(mainQueue, ^{
                    NSError *err = [NSError errorWithDomain:@"sendCommandError" code:0 userInfo:@{NSLocalizedDescriptionKey: @"lamps array is empty."}];
                    completion(err);
                });
            }
            
            return;
        }
        
        for (LeadLamp *lamp in lamps) {
            if (lamp.ip) {
                GCDAsyncSocket *sock = weakSelf.connectedSocketsDictionary[lamp.ip];
                if (sock && ![sock isConnected]) {
                    sock.delegate = self;
                    NSString *actualIP = [weakSelf actualIPv4OrIPv6WithIP:lamp.ip port:SR_TCP_PORT];
                    NSError *err;
                    [sock connectToHost:actualIP onPort:SR_TCP_PORT error:&err];
                    if (err) {
                        NSLog(@"connectToHost %@ err: %@", lamp.ip, err);
                    }
                    
                    [NSThread sleepForTimeInterval:0.5];
                }
                
                for (NSData *data in cmd.datas) {
                    NSLog(@"send %@ to %@", data, lamp.ip);
                    // 3 times send
                    for (int i = 0; i < 3; i++) {
                        [sock writeData:data withTimeout:-1 tag:0];
                    }
                    [NSThread sleepForTimeInterval:0.1];
                }
            }
        }
        
        if (completion) {
            dispatch_async(mainQueue, ^{
                completion(nil);
            });
        }
    });
}

#pragma mark - GCDAsyncSocketDelegate

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    [_connectedSocketsDictionary setObject:sock forKey:host];
}

#pragma mark - GCDAsyncUdpSocketDelegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext {
    if (!address) { return; }
    
    NSString *host = [GCDAsyncUdpSocket hostFromAddress:address];
    NSArray *hostComps = [host componentsSeparatedByString:@":"];
    if (hostComps.count > 0) {
        host = hostComps.lastObject;
    }
    
    if (_localIP && [_localIP isEqualToString:host]) {
        return;
    }
    
    NSString *receiveString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"udp recv: %@ : %@", receiveString, host);
    
    if (!receiveString) {
        return;
    }
    
    switch (_udpTag) {
        case SRUDPTag_SearchLeadLamps: {
            NSArray *receiveStringComps = [receiveString componentsSeparatedByString:@","];
            
            if (receiveStringComps.count == 3) {
                NSString *ip = receiveStringComps[0];
                NSString *mac = receiveStringComps[1];
                
                if (_foundLampsDictionary[ip]) {
                    break;
                }
                
                LeadLamp *lamp = [[LeadLamp alloc] init];
                lamp.ip = ip;
                lamp.mac = mac;
                
                [_foundLampsDictionary setObject:lamp forKey:ip];
            }
        
            break;
        }
        case SRUDPTag_SearchWifiNetworksNearby: {
            if (receiveString.length > 8) {
                NSArray *comps = [receiveString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                for (int i = 0; i < comps.count; i++) {
                    NSString *networkStr = comps[i];
                    WifiNetwork *network = [self networkFromString:networkStr];
                    if (network) {
                        [_nearybyNetworksDictionary setObject:network forKey:network.macAddress];
                    }
                }
            }
        
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Private

- (void)sendUdp:(NSString *)data times:(int)times tag:(long)tag {
    [self sendUdp:data times:times delay:0 tag:tag];
}

- (void)sendUdp:(NSString *)data times:(int)times delay:(NSTimeInterval)delay tag:(long)tag {
    
    if (!data || times < 0) {
        return;
    }
    
    NSData *sendData = [data dataUsingEncoding:NSASCIIStringEncoding];
    
    if (!sendData) {
        return;
    }
    
    if (delay > 0) {
        [NSThread sleepForTimeInterval:delay];
    }
    
    _udpTag = tag;
    
    NSLog(@"Send udp data %@, times %d, delay %tu, tag %ld", data, times, delay, tag);
    
    for (int i = 0; i < times; i++) {
        if (_udpSocket) {
            [_udpSocket sendData:sendData toHost:_udpIP port:SR_UDP_PORT withTimeout:-1 tag:tag];
        } else {
            break;
        }
        
        [NSThread sleepForTimeInterval:0.05];
    }
}

- (BOOL)resetUdp {
    _udpIP = nil;
    
    if (![self localSSID]) {
        NSLog(@"No conenction to Wifi");
        return false;
    }
    
    NSString *localIP = [self localIP];
    
    if (!localIP || localIP.length <= 0) {
        NSLog(@"No correct IP! %@", localIP);
        return false;
    }
    
    NSArray *ipComps = [localIP componentsSeparatedByString:@"."];
    if (ipComps && ipComps.count > 0) {
        _udpIP = @"";
        for (int i = 0; i < ipComps.count - 1; i++) {
            NSString *comp = ipComps[i];
            _udpIP = [_udpIP stringByAppendingFormat:@"%@.", comp];
        }
        _udpIP = [_udpIP stringByAppendingString:@"255"];
    }
    
    NSLog(@"udpIP %@", _udpIP);
    
    if (!_udpIP) {
        NSLog(@"udpIP is nil");
        return false;
    }
    
    if (_udpSocket) {
        [_udpSocket close];
        _udpSocket = nil;
        // Make sure udpSokcet closed
        [NSThread sleepForTimeInterval:1.5];
    }
    
    _udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    
    NSError *err = nil;
    
    [_udpSocket enableBroadcast:YES error:&err];
    if (err) {
        NSLog(@"enableBroadcast err: %@", err);
        return false;
    }
    
    [_udpSocket bindToPort:SR_UDP_PORT error:&err];
    if (err) {
        NSLog(@"bindToPort %tu err: %@", SR_UDP_PORT, err);
        return false;
    }
    
    [_udpSocket beginReceiving:&err];
    if (err) {
        NSLog(@"beginReceiving err: %@", err);
        return false;
    }
    
    return true;
}

- (void)disconnectAllSockets {
    for (GCDAsyncSocket *sock in _connectedSocketsDictionary.allValues) {
        if (sock && [sock isConnected]) {
            [sock disconnect];
            sock.delegate = nil;
        }
    }
    [_connectedSocketsDictionary removeAllObjects];
    [_foundLampsDictionary removeAllObjects];
    [_nearybyNetworksDictionary removeAllObjects];
}

- (void)startHeartBeat {
    dispatch_async(mainQueue, ^{
        _heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(sendHeartBeat) userInfo:nil repeats:true];
    });
}

- (void)sendHeartBeat {
    __weak typeof(self) weakSelf = self;
    dispatch_async(managerConcurrentQueue, ^{
        Byte bytes[] = {0xFF};
        NSData *heartBeatData = [NSData dataWithBytes:bytes length:1];
        for (NSString *ip in weakSelf.connectedSocketsDictionary.allKeys) {
            GCDAsyncSocket *sock = weakSelf.connectedSocketsDictionary[ip];
            if (sock && ![sock isConnected]) {
                NSError *err = nil;
                NSString *actualIP = [weakSelf actualIPv4OrIPv6WithIP:ip port:SR_TCP_PORT];
                sock.delegate = weakSelf;
                [sock connectToHost:actualIP onPort:SR_TCP_PORT error:&err];
                if (err) {
                    NSLog(@"connectToHost err: %@", err);
                }
                [NSThread sleepForTimeInterval:0.5];
            }
            
            [sock writeData:heartBeatData withTimeout:-1 tag:0];
            [NSThread sleepForTimeInterval:0.05];
            
            NSLog(@"Send heart beat to %@", ip);
        }
    });
}

- (void)stopHeartBeat {
    if (_heartBeatTimer) {
        [_heartBeatTimer invalidate];
        _heartBeatTimer = nil;
    }
}


- (NSString *)localIP {
    _localIP = nil;
    
    struct ifaddrs *interfaces = nil;
    struct ifaddrs *temp_add = nil;
    
    int success = 0;
    
    success = getifaddrs(&interfaces);
    
    if (success == 0) {
        temp_add = interfaces;
        
        while (temp_add != nil) {
            if (temp_add->ifa_addr->sa_family == AF_INET) {
                if ([[NSString stringWithUTF8String:temp_add->ifa_name] isEqualToString:@"en0"]) {
                    _localIP = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_add->ifa_addr)->sin_addr)];
                }
            }
            temp_add = temp_add->ifa_next;
        }
    }
    
    freeifaddrs(interfaces);
    
    return _localIP;
}

- (NSString *)actualIPv4OrIPv6WithIP:(NSString *)ip port:(NSInteger)port {
    NSError *addresseError = nil;
    NSArray *addresseArray = [GCDAsyncSocket lookupHost:ip port:port error:&addresseError];
    if (addresseError) {
        NSLog(@"");
    }
    
    NSString *actualIP = @"";
    for (NSData *addrData in addresseArray) {
        if ([GCDAsyncSocket isIPv6Address:addrData]) {
            actualIP = [GCDAsyncSocket hostFromAddress:addrData];
        }
    }
    
    if (actualIP.length == 0) {
        actualIP = ip;
    }
    
    return actualIP;
}

- (NSString *)localSSID {
    NSString *WiFiName = nil;
    
    CFArrayRef WiFiInterfaces = CNCopySupportedInterfaces();
    
    if (!WiFiInterfaces) {
        return WiFiName;
    }
    
    NSArray *interfaces = (__bridge NSArray *)WiFiInterfaces;
    
    for (NSString *interfaceName in interfaces) {
        CFDictionaryRef dictionaryRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName);
        
        if (dictionaryRef) {
            NSDictionary *newworkInfo = (__bridge NSDictionary *)dictionaryRef;
            
            WiFiName = [newworkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            
            CFRelease(dictionaryRef);
        }
    }
    
    CFRelease(WiFiInterfaces);
    
    return WiFiName;
}

- (WifiNetwork *)networkFromString:(NSString *)str {
    WifiNetwork *network = nil;
    
    if (str.length < 8) {
        return network;
    }
    
    NSArray *comps = [str componentsSeparatedByString:@","];
    if (comps.count >= 5) {
        NSString *mac = comps[2];
        
        if (mac.length == 17) {
            network = [[WifiNetwork alloc] init];
            
            NSString *channel = comps[0];
            NSString *name = comps[1];
            NSString *security = comps[3];
            NSString *signal = comps[4];
            
            network.channel = channel;
            network.ssid = name ? name : network.ssid;
            network.macAddress = mac;
            network.securityType = security;
            network.signal = signal;
        }
    }

    
    return network;
}

@end
