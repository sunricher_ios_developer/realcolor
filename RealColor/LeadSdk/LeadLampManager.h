//
//  LeadLampManager.h
//  LeadLedDemo
//
//  Created by wangwendong on 2017/3/23.
//  Copyright © 2017年 Sunricher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LeadLampCommand.h"

#pragma mark - LeadLamp

@interface LeadLamp : NSObject

@property (strong, nonatomic) NSString *mac;
@property (strong, nonatomic) NSString *ip;
@property (strong, nonatomic) NSString *name;

@end

#pragma mark - WifiNetwork

@interface WifiNetwork : NSObject

@property (nonatomic, strong) NSString *channel;
@property (nonatomic, strong) NSString *ssid;
@property (nonatomic, strong) NSString *macAddress;
@property (nonatomic, strong) NSString *securityType;
@property (nonatomic, strong) NSString *signal;

@end

#pragma mark - LeadLampManager

@interface LeadLampManager : NSObject

+ (instancetype)sharedInstance;

- (NSArray<LeadLamp *> *)connectedLeadLamps;

- (void)searchLeadLamps:(void(^)(NSArray<LeadLamp *> *lamps))completion;

- (BOOL)isConnectedLeadLampDirectly;

- (void)searchWifiNetworksNearby:(void(^)(NSArray<WifiNetwork *> *networks))completion;

- (void)connectToTheWifiNetwork:(WifiNetwork *)network passwork:(NSString *)password completed:(void(^)(NSError *err))completion;

- (void)restoreToFactorySettings:(void(^)(NSError *err))completion;

- (void)sendCommand:(LeadLampCommand *)cmd toLamp:(LeadLamp *)lamp completion:(void(^)(NSError *err))completion;
- (void)sendCommand:(LeadLampCommand *)cmd toLamps:(NSArray<LeadLamp *> *)lamps completion:(void(^)(NSError *err))completion;

@end
