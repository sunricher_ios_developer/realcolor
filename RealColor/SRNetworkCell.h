//
//  SRNetworkCell.h
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WifiNetwork;

@interface SRNetworkCell : UITableViewCell

- (void)setupCellInfo:(WifiNetwork *)network;

@end
