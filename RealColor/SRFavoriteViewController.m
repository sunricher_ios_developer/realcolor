//
//  SRFavoriteViewController.m
//  RealColor
//
//  Created by sunhong on 2017/3/28.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import "SRFavoriteViewController.h"
#import "SRFavoriteCell.h"
#import "SRDataBaseManger.h"
#import "LeadLampManager.h"
#import "LeadLampCommand.h"

@interface SRFavoriteViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *dataSource;
@property (nonatomic, strong) NSIndexPath *selectIndexPath;

@end

@implementation SRFavoriteViewController

- (UITableView *)tableView{
    if(!_tableView){
        CGFloat topHeight = ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 88 : 64);
        CGFloat  BottomHeight =  ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896 ? 83 : 49);
        UITableView *tb = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - BottomHeight - topHeight) style:UITableViewStyleGrouped];
        tb.delegate = self;
        tb.dataSource = self;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:tb.bounds];
        imageView.image = [UIImage imageNamed:@"background"];
        tb.backgroundView = imageView;
        tb.separatorColor = [UIColor colorWithRed:65/255.0 green:65/255.0 blue:65/255.0 alpha:1.0];
        _tableView = tb;
        [_tableView registerNib:[UINib nibWithNibName:@"SRFavoriteCell" bundle:nil] forCellReuseIdentifier:@"SRFavoriteCell"];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self tableView];

    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    _dataSource = [NSMutableDictionary dictionary];
    _dataSource = [NSMutableDictionary dictionaryWithDictionary:[[SRDataBaseManger sharedInstance] getSceneInfo]];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark -- keybord notification

- (void)keyboardWasShown:(NSNotification *)aNotification{
    if(self.selectIndexPath){
        [self.tableView selectRowAtIndexPath:_selectIndexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification{
    _selectIndexPath = nil;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark -- tableview delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]){
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SRFavoriteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SRFavoriteCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *scenName = [_dataSource objectForKey:@(indexPath.row + 1)];
    if(scenName.length == 0){
        scenName = [NSString stringWithFormat:NSLocalizedString(@"Scene_name", nil),indexPath.row + 1];
    }
    cell.sceneTF.text = scenName;
    NSArray *lamps = [[SRDataBaseManger sharedInstance] selectedLampsArray];
    [cell setSaveBlock:^{
        LeadLampCommand *commd = [LeadLampCommand saveSceneAtIndex:indexPath.row + 1];
        [[LeadLampManager sharedInstance] sendCommand:commd toLamps:lamps completion:^(NSError *err) {
            if(err){
                NSLog(@"%@",err);
            }
        }];
        
    }];
    [cell setTriggerBlock:^{
        LeadLampCommand *cmd = [LeadLampCommand triggerSceneAtIndex:indexPath.row + 1];
        [[LeadLampManager sharedInstance] sendCommand:cmd toLamps:lamps completion:^(NSError *err) {
            
        }];
    }];
    __weak typeof(self) weakSelf = self;
    [cell setScneNameBlock:^(SRFavoriteCell *favoriteCell,NSString *name) {
        if([name isEqualToString:scenName] || name.length == 0){
            favoriteCell.sceneTF.text = scenName;
        }else{
            [[SRDataBaseManger sharedInstance] updateScenInfo:name index:indexPath.row + 1];
            weakSelf.dataSource[@(indexPath.row + 1)] = name;
        }
    }];
    
    [cell setScenNameTFBeginEditBlock:^{
        weakSelf.selectIndexPath = indexPath;
    }];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return self.view.frame.size.height - 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.tableView endEditing:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.tableView endEditing:YES];
}

@end
