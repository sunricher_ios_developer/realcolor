//
//  SRFavoriteCell.h
//  RealColor
//
//  Created by sunhong on 2017/3/29.
//  Copyright © 2017年 sunhong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRFavoriteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *sceneTF;
@property (nonatomic, copy) void(^saveBlock)();
@property (nonatomic, copy) void(^triggerBlock)();
@property (nonatomic, copy) void(^scneNameBlock)(SRFavoriteCell *cell,NSString *name);
@property (nonatomic, copy) void(^scenNameTFBeginEditBlock)();

@end
